import { Component } from "@angular/core";

import { Platform, NavController } from "@ionic/angular";
import { SplashScreen } from "@ionic-native/splash-screen/ngx";
import { StatusBar } from "@ionic-native/status-bar/ngx";
import { Router } from "@angular/router";

@Component({
  selector: "app-root",
  templateUrl: "app.component.html",
  styleUrls: ["app.component.scss"]
})
export class AppComponent {
  public appPages = [
    {
      title: "Home",
      url: "/home",
      icon: "home"
    },
    {
      title: "Attendance",
      url: "/attendance",
      icon: "calendar"
    },
    {
      title: "Course",
      url: "/course",
      icon: "bookmarks"
    },
    {
      title: "Lectures",
      url: "/lecture",
      icon: "book"
    },
    {
      title: "Important Notes",
      url: "/important-notes",
      icon: "alert"
    }
  ];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router,
    public nav: NavController
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleLightContent();
      this.splashScreen.hide();
    });
  }

  backButtonEventListener() {
    this.platform.backButton.subscribeWithPriority(0, () => {
      this.navigateToPreviousPage();
    });
  }

  navigateToPreviousPage() {
    // https://link.medium.com/Zy0YtQDTSY
    const url = this.router.url;
    if (url === "/home" && this.platform.is("cordova")) {
      navigator["app"].exitApp();
    } else {
      this.nav.navigateBack(
        url.replace(new RegExp("(/([a-zA-Z0-9-.])*)$"), "")
      );
    }
  }
}
