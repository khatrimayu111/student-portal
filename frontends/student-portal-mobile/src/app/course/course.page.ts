import { Component, OnInit } from "@angular/core";
import { NavController, AlertController } from "@ionic/angular";

@Component({
  selector: "app-course",
  templateUrl: "./course.page.html",
  styleUrls: ["./course.page.scss"]
})
export class CoursePage implements OnInit {
  selectedSubject: {
    name: string;
    prof: string;
    roomNo: string;
    assignments: Array<{ title: string; description: string }>;
  };
  subjects: Array<{
    name: string;
    prof: string;
    roomNo: string;
    assignments: Array<{ title: string; description: string }>;
  }>;

  constructor(
    private navCtrl: NavController,
    private alertController: AlertController
  ) {}

  ngOnInit() {
    this.selectedSubject = {} as {
      name: string;
      prof: string;
      roomNo: string;
      assignments: Array<{ title: string; description: string }>;
    };
    this.selectedSubject.name = "";
    this.selectedSubject.prof = "";
    this.selectedSubject.roomNo = "";
    this.subjects = [
      {
        name: "Physics",
        prof: "Prof. Albert Einstien",
        roomNo: "201",
        assignments: [
          {
            title: "Assignment 1",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Assignment 2",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Unit test 18-11",
            description: "Submission date: 22-12-2019"
          }
        ]
      },
      {
        name: "Programming",
        prof: "Osama Bin Laden",
        roomNo: "202",
        assignments: [
          {
            title: "Assignment 1",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Assignment 2",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Unit test 18-11",
            description: "Submission date: 22-12-2019"
          }
        ]
      },
      {
        name: "Mathematics",
        prof: "Prof. Prafful Sir",
        roomNo: "203",
        assignments: [
          {
            title: "Assignment 1",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Assignment 2",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Unit test 18-11",
            description: "Submission date: 22-12-2019"
          }
        ]
      },
      {
        name: "Chemistry",
        prof: "Mr. Myuddin",
        roomNo: "204",
        assignments: [
          {
            title: "Assignment 1",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Assignment 2",
            description: "Submission date: 20-11-2019"
          },
          {
            title: "Unit test 18-11",
            description: "Submission date: 22-12-2019"
          }
        ]
      }
    ];
  }

  async openPdf() {
    const alert = await this.alertController.create({
      header: "Syllabus",
      message: "Syllabus PDF will be downloaded from here.",
      buttons: [
        {
          text: "Okay",
          handler: () => {
            console.log("Confirm Okay");
          }
        }
      ]
    });
    await alert.present();
  }

  async showAssignment(assignment) {
    const alert = await this.alertController.create({
      header: "Assignment",
      message: `The ${assignment.title} will be downloaded from here.`,
      buttons: [
        {
          text: "Okay",
          handler: () => {
            console.log("Confirm Okay");
          }
        }
      ]
    });
    await alert.present();
  }

  selectSubject(name: string) {
    this.subjects.forEach(subject => {
      if (subject.name === name) this.selectedSubject = subject;
    });
  }

  clear() {
    delete this.selectedSubject;
    this.selectedSubject = {} as {
      name: string;
      prof: string;
      roomNo: string;
      assignments: Array<{ title: string; description: string }>;
    };
    this.selectedSubject.name = "";
    this.selectedSubject.prof = "";
    this.selectedSubject.roomNo = "";
  }

  navigateBack() {
    this.navCtrl.pop();
  }
}
