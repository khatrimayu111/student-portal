import { NgModule } from "@angular/core";
import { PreloadAllModules, RouterModule, Routes } from "@angular/router";
import { LecturePage } from "./lecture-ui/lecture/lecture.page";
import { CurrentLecturePage } from "./lecture-ui/current-lecture/current-lecture.page";

const routes: Routes = [
  {
    path: "home",
    loadChildren: () => import("./home/home.module").then(m => m.HomePageModule)
  },
  {
    path: "lecture",
    component: LecturePage,
    children: [
      {
        path: "current-lecture",
        loadChildren:
          "./lecture-ui/current-lecture/current-lecture.module#CurrentLecturePageModule"
      },
      {
        path: "live-lecture",
        loadChildren:
          "./lecture-ui/live-lecture/live-lecture.module#LiveLecturePageModule"
      },
      {
        path: "previous-lecture",
        loadChildren:
          "./lecture-ui/previous-lecture/previous-lecture.module#PreviousLecturePageModule"
      }
    ]
  },
  {
    path: "",
    redirectTo: "home",
    pathMatch: "full"
  },
  { path: 'course', loadChildren: './course/course.module#CoursePageModule' },
  { path: 'important-notes', loadChildren: './important-notes/important-notes.module#ImportantNotesPageModule' },
  { path: 'attendance', loadChildren: './attendance/attendance.module#AttendancePageModule' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
