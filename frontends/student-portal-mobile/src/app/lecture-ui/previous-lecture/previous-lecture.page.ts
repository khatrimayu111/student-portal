import { Component, OnInit } from "@angular/core";
import { AlertController } from "@ionic/angular";

@Component({
  selector: "app-previous-lecture",
  templateUrl: "./previous-lecture.page.html",
  styleUrls: ["./previous-lecture.page.scss"]
})
export class PreviousLecturePage implements OnInit {
  selectedSubject: {
    name: string;
    description: string;
    lectures: Array<{
      topic: string;
      date: string;
      class: string;
      imgUrl: string;
    }>;
  };
  subjects: Array<{
    name: string;
    description: string;
    lectures: Array<{
      topic: string;
      date: string;
      class: string;
      imgUrl: string;
    }>;
  }>;

  constructor(private alertController: AlertController) {}

  ngOnInit() {
    this.selectedSubject = {} as {
      name: string;
      description: string;
      lectures: Array<{
        topic: string;
        date: string;
        class: string;
        imgUrl: string;
      }>;
    };
    this.selectedSubject.name = "";
    this.selectedSubject.description = "";
    this.selectedSubject.lectures = [];
    this.subjects = [
      {
        name: "Physics",
        description:
          "Physics is the study of the natural world. It deals with the fundamental particles of which the universe is made, and the interactions between those particles, the objects composed of them (nuclei, atoms, molecules, etc) and energy. Physics is the science of Nature - of matter and energy in space and time.",
        lectures: [
          {
            topic: "Circular motion",
            class: "FE-IT",
            date: "14-Nov",
            imgUrl:
              "https://d1whtlypfis84e.cloudfront.net/guides/wp-content/uploads/2018/02/21121639/2000px-Circular_motion_velocity_and_acceleration2.svg_-300x300.png"
          },
          {
            topic: "Wave Optics",
            class: "FE-IT",
            date: "13-Nov",
            imgUrl:
              "https://gs-post-images.grdp.co/2018/12/capture-img1545201201232-32.PNG-rs-high-webp.PNG"
          }
        ]
      },
      {
        name: "Programming",
        description:
          "Computer programmers write code to create software programs. They turn the program designs created by software developers and engineers into instructions that a computer can follow. Programmers must debug the programs—that is, test them to ensure that they produce the expected results.",
        lectures: [
          {
            topic: "AngularJS",
            class: "TE-IT",
            date: "14-Nov",
            imgUrl:
              "https://banner2.cleanpng.com/20180420/sxw/kisspng-angularjs-ruby-on-rails-typescript-web-application-icon-hacker-5ad97b80139367.5630065415242023680802.jpg"
          },
          {
            topic: "JAVA",
            class: "TE-IT",
            date: "13-Nov",
            imgUrl:
              "https://mpng.pngfly.com/20180805/iot/kisspng-logo-java-runtime-environment-programming-language-java-util-concurrentmodificationexception-Ömer-5b6766ab2d98b8.1809687115335031471868.jpg"
          }
        ]
      },
      {
        name: "Mathematics",
        description:
          "The abstract science of number, quantity, and space, either as abstract concepts ( pure mathematics ), or as applied to other disciplines such as physics and engineering ( applied mathematics ).",
        lectures: [
          {
            topic: "Integration",
            class: "TE-IT",
            date: "14-Nov",
            imgUrl:
              "https://i0.wp.com/www.tuitionwithjason.sg/wp-content/uploads/2019/04/a-math-integration-fraction-and-ln.png?fit=442%2C246&ssl=1"
          },
          {
            topic: "Matrices",
            class: "TE-IT",
            date: "13-Nov",
            imgUrl:
              "https://miro.medium.com/max/1164/1*O7c9ZZ0jJnOAYrRJYmENvw.png"
          }
        ]
      },
      {
        name: "Chemistry",
        description:
          "Chemistry is the science of matter at or near the atomic scale. (Matter is the substance of which all physical objects are made.) Chemistry deals with the properties of matter, and the transformation and interactions of matter and energy.",
        lectures: [
          {
            topic: "Organic chemistry",
            class: "TE-IT",
            date: "14-Nov",
            imgUrl:
              "https://png.pngtree.com/element_origin_min_pic/17/08/05/0c576f521ace9afdd7f26765976e061c.jpg"
          },
          {
            topic: "In-organic chemistry",
            class: "TE-IT",
            date: "13-Nov",
            imgUrl:
              "https://kids.kiddle.co/images/thumb/8/8e/Potassium-oxide-3D-vdW.png/471px-Potassium-oxide-3D-vdW.png"
          }
        ]
      }
    ];
  }

  selectSubject(name: string) {
    this.subjects.forEach(subject => {
      if (subject.name === name) this.selectedSubject = subject;
    });
  }

  clear() {
    delete this.selectedSubject;
    this.selectedSubject = {} as {
      name: string;
      description: string;
      lectures: Array<{
        topic: string;
        date: string;
        class: string;
        imgUrl: string;
      }>;
    };
    this.selectedSubject.name = "";
    this.selectedSubject.description = "";
    this.selectedSubject.lectures = [];
  }

  async alert() {
    const alert = await this.alertController.create({
      header: "Coming soon",
      message: "Video feature coming soon",
      buttons: ["OK"]
    });

    await alert.present();
  }
}
