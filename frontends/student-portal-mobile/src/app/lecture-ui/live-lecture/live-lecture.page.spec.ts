import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiveLecturePage } from './live-lecture.page';

describe('LiveLecturePage', () => {
  let component: LiveLecturePage;
  let fixture: ComponentFixture<LiveLecturePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiveLecturePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiveLecturePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
