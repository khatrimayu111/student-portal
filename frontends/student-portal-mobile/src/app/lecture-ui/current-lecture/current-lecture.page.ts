import { Component, OnInit } from "@angular/core";
import { VideoPlayer, VideoOptions } from "@ionic-native/video-player/ngx";
import { LoadingController } from "@ionic/angular";

@Component({
  selector: "app-current-lecture",
  templateUrl: "./current-lecture.page.html",
  styleUrls: ["./current-lecture.page.scss"]
})
export class CurrentLecturePage implements OnInit {
  selectedSubject: { name: string; description: string };
  subjects: Array<{ name: string; description: string }>;
  videoOpts: VideoOptions;

  constructor(
    private videoPlayer: VideoPlayer,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.selectedSubject = {} as { name: string; description: string };
    this.selectedSubject.name = "";
    this.selectedSubject.description = "";
    this.subjects = [
      {
        name: "Physics",
        description:
          "Physics is the study of the natural world. It deals with the fundamental particles of which the universe is made, and the interactions between those particles, the objects composed of them (nuclei, atoms, molecules, etc) and energy. Physics is the science of Nature - of matter and energy in space and time."
      },
      {
        name: "Programming",
        description:
          "Computer programmers write code to create software programs. They turn the program designs created by software developers and engineers into instructions that a computer can follow. Programmers must debug the programs—that is, test them to ensure that they produce the expected results."
      },
      {
        name: "Mathematics",
        description:
          "The abstract science of number, quantity, and space, either as abstract concepts ( pure mathematics ), or as applied to other disciplines such as physics and engineering ( applied mathematics )."
      },
      {
        name: "Chemistry",
        description:
          "Chemistry is the science of matter at or near the atomic scale. (Matter is the substance of which all physical objects are made.) Chemistry deals with the properties of matter, and the transformation and interactions of matter and energy."
      }
    ];
  }

  async playVideo() {
    const loading = await this.loadingController.create({
      message: "Loading Video ........",
      spinner: "bubbles",
      duration: 500
    });
    await loading.present();
    this.videoOpts = { volume: 1.0 };
    this.videoPlayer
      .play(
        "https://mntechnique.sgp1.digitaloceanspaces.com/aartzy/Supercooled%20Water%20-%20Explained!.mp4"
      )
      .then(async () => {})
      .catch(err => {
        console.log(err);
      });
  }

  stopPlayingVideo() {
    this.videoPlayer.close();
  }

  selectSubject(name: string) {
    this.subjects.forEach(subject => {
      if (subject.name === name) this.selectedSubject = subject;
    });
  }

  clear() {
    delete this.selectedSubject;
    this.selectedSubject = {} as { name: string; description: string };
    this.selectedSubject.name = "";
    this.selectedSubject.description = "";
    console.log(this.subjects);
  }
}
