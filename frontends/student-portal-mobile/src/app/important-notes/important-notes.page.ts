import { Component, OnInit } from "@angular/core";
import { NavController } from "@ionic/angular";

@Component({
  selector: "app-important-notes",
  templateUrl: "./important-notes.page.html",
  styleUrls: ["./important-notes.page.scss"]
})
export class ImportantNotesPage implements OnInit {
  selectedSubject: {
    name: string;
    notes: Array<{ title: string; date: string }>;
    attachments: Array<{ title: string }>;
  };
  subjects: Array<{
    name: string;
    notes: Array<{ title: string; date: string }>;
    attachments: Array<{ title: string }>;
  }>;
  displayList: string;
  constructor(private navCtrl: NavController) {}

  ngOnInit() {
    this.displayList = "notes";
    this.selectedSubject = {} as {
      name: string;
      notes: Array<{ title: string; date: string }>;
      attachments: Array<{ title: string }>;
    };
    this.selectedSubject.name = "";
    this.subjects = [
      {
        name: "Physics",
        notes: [
          {
            title: "https://www.w3schools.com/",
            date: "20-11-2019"
          },
          {
            title: `following formulas are important for integration.a^2 – b^2 = (a – b)(a + b)`,
            date: "20-11-2019"
          },
          {
            title: "Unit test 18-11",
            date: "22-12-2019"
          }
        ],
        attachments: [
          { title: "Question Bank - Unit test 1" },
          {
            title: "Important questions - Circular motion"
          }
        ]
      },
      {
        name: "Programming",
        notes: [
          {
            title: "Refer to link below for html basic tags",
            date: "20-11-2019"
          },
          {
            title: "https://www.w3schools.com/html/default.asp",
            date: "20-11-2019"
          },
          {
            title: "Unit test 18-11",
            date: "22-12-2019"
          }
        ],
        attachments: [
          { title: "Question Bank - Unit test 2" },
          {
            title: "Important questions - Microservices"
          },
          {
            title: "Java"
          }
        ]
      },
      {
        name: "Mathematics",
        notes: [
          {
            title: "https://www.w3schools.com/html/default.asp",
            date: "20-11-2019"
          },
          {
            title: "1+1 = 2",
            date: "20-11-2019"
          },
          {
            title: "Unit test 18-11",
            date: "22-12-2019"
          }
        ],
        attachments: [
          { title: "FAQ's" },
          {
            title: "Formulas - Integration"
          },
          {
            title: "Formulas - Derivatives"
          }
        ]
      },
      {
        name: "Chemistry",
        notes: [
          {
            title: "https://www.w3schools.com/html/default.asp",
            date: "20-11-2019"
          },
          {
            title: "C + O2 = CO2",
            date: "20-11-2019"
          },
          {
            title: "Unit test 18-11",
            date: "22-12-2019"
          }
        ],
        attachments: [
          { title: "Chemical equations" },
          {
            title: "Question paper"
          }
        ]
      }
    ];
  }

  selectSubject(name: string) {
    this.subjects.forEach(subject => {
      if (subject.name === name) this.selectedSubject = subject;
    });
  }

  clear() {
    delete this.selectedSubject;
    this.selectedSubject = {} as {
      name: string;
      notes: Array<{ title: string; date: string }>;
      attachments: Array<{ title: string }>;
    };
    this.selectedSubject.name = "";
  }

  navigateBack() {
    this.navCtrl.pop();
  }
}
