import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ImportantNotesPage } from './important-notes.page';

describe('ImportantNotesPage', () => {
  let component: ImportantNotesPage;
  let fixture: ComponentFixture<ImportantNotesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ImportantNotesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ImportantNotesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
