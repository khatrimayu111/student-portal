import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-attendance",
  templateUrl: "./attendance.page.html",
  styleUrls: ["./attendance.page.scss"]
})
export class AttendancePage implements OnInit {
  selectedSubject: {
    name: string;
    attendance: string;
  };
  subjects: Array<{
    name: string;
    attendance: string;
  }>;
  month: string;
  constructor() {}

  ngOnInit() {
    this.month = "nov";
    this.selectedSubject = {} as {
      name: string;
      attendance: string;
    };
    this.selectedSubject.name = "";
    this.subjects = [
      {
        name: "Physics",
        attendance: "75%"
      },
      {
        name: "Programming",
        attendance: "50%"
      },
      {
        name: "Mathematics",
        attendance: "100%"
      },
      {
        name: "Chemistry",
        attendance: "25%"
      }
    ];
  }
  selectSubject(name: string) {
    this.subjects.forEach(subject => {
      if (subject.name === name) this.selectedSubject = subject;
    });
  }
  clear() {
    delete this.selectedSubject;
    this.selectedSubject = {} as {
      name: string;
      attendance: string;
    };
    this.selectedSubject.name = "";
  }
}
